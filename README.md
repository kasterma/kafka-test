# Kafka-test

(kafka documentation)[http://kafka.apache.org/documentation.html]

Vagrantfile contains the setup of a kafka enabled virtual system as
suggested in the quickstart of the documentation.

Note: vagrant up doesn't quite succeed yet.

Note: the partitioner should consider the hash of keys, but if the
keys are bytearrays this has is based on the identity of the object
(this is the standard impl, not required by the standard).  In the
trunk for kafka there is ByteArrayPartitioner that will solve this.
It is not clear why I need to use p/message only with arrays of bytes.