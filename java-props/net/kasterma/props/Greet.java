package net.kasterma.props;

// example from http://www.configure-all.com/java_properties.php

import java.io.*;
import java.util.*;

public class Greet
{
    String message;

    // class constructor

    public Greet()
    {

    }

    public void setMessage()
    {

	//create an instance of properties class

	Properties props = new Properties();

	//try retrieve data from file
	try {

	    props.load(new FileInputStream("message.properties"));

	    message = props.getProperty("message");

	    System.out.println(message);
	}

	//catch exception in case properties file does not exist

	catch(IOException e)
	    {
		e.printStackTrace();
	    }
    }

    public static void main(String[] args)
    {
	//create an instance of greeting2 class

	Greet gr = new Greet();

	//call the setMessage() method of the Greeting2 class

	gr.setMessage();

    }
} 