# setup kafka on the new image

sudo apt-get update
sudo apt-get install -y openjdk-7-jdk emacs23 git tmux
cd /tmp
wget https://dist.apache.org/repos/dist/release/kafka/kafka-0.8.0-beta1-src.tgz
tar xzf kafka-0.8.0-beta1-src.tgz
cd kafka-0.8.0-beta1-src/
./sbt update
./sbt package
./sbt assembly-package-dependency
sudo -u vagrant ln -s /vagrant/java-props/ /home/vagrant/java-props
sudo -u vagrant ln -s /vagrant/kt /home/vagrant/kt

# to get emacs24 need to add ppa:cassou, for this need python-software-properties
sudo apt-get install -y python-software-properties
sudo add-apt-repository ppa:cassou/emacs
sudo apt-get update
sudo apt-get install -y emacs24
# get basics needed for this image
sudo apt-get install -y sqlite3 openjdk-7-jdk
# get lein and link project files
sudo -u vagrant mkdir /home/vagrant/bin
sudo -u vagrant wget https://raw.github.com/technomancy/leiningen/stable/bin/lein
sudo -u vagrant chmod a+x lein
sudo -u vagrant mv lein /home/vagrant/bin/
sudo -u vagrant ln -s /vagrant/kt /home/vagrant/kt
# put dotfiles and related in place
sudo -u vagrant cp /vagrant/dotfiles/dotprofile /home/vagrant/.profile
sudo -u vagrant cp /vagrant/dotfiles/dotemacs .emacs
sudo -u vagrant cp /vagrant/dotfiles/dottmux.conf .tmux.conf
sudo -u vagrant cp -r /vagrant/dotemacs.d/* .emacs.d

